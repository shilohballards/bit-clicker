import React, { Component } from 'react';
import Game from './Components/Game';
import Login from './Components/Login';
import CosmeticMenu from './Components/CosmeticMenu';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

class App extends Component {
  constructor(props) {
    super(props);
    this.state={};
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/game" component={Game} />
          <Route path="/cosmetics" component={CosmeticMenu} />
        </Switch>
      </BrowserRouter>
    );
  }
  
}

export default App;
