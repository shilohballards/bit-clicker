import React, { Component } from 'react';
import Item from './Item';
import Upgrades from './Upgrades.json';


class UpgradeList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

   

    render() {

        const upgradeList = Upgrades.map(upgrade => 
            <Item upgrade={upgrade} 
                mouseFunc={this.props.mouseFunc}
                mouseCount={this.props.mouseCount}
                macroFunc={this.props.macroFunc}
                macroCount={this.props.macroCount}
                internFunc={this.props.internFunc}
                internCount={this.props.internCount}
                botFunc={this.props.botFunc}
                botCount={this.props.botCount}
                devTeamFunc={this.props.devTeamFunc}
                devTeamCount={this.props.devTeamCount}
                devFunc={this.props.devFunc}
                devCount={this.props.devCount}
                techFunc={this.props.techFunc}
                techCount={this.props.techCount}
                superFunc={this.props.superFunc}
                superCount={this.props.superCount}
                quanFunc={this.props.quanFunc}
                quanCount={this.props.quanCount}
                bits={this.props.bits}
                memory={this.props.memory} />
        );

        return (
        
        <div className="rendered-list">
            {upgradeList}
        </div>
        
        );
    }
}


export default UpgradeList;