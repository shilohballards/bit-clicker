import React, { Component } from 'react';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    
    render() {
        return (
            <div className="rendered-item">
                <button title={this.props.upgrade.cost} className="item" onClick={this.props[this.props.upgrade.func]}>{this.props.upgrade.name}</button>
                <p>{this.props.upgrade.label} {this.props[this.props.upgrade.count]}</p>
            </div>
        );
    }
}

export default Item;