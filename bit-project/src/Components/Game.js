import React, { Component } from 'react';
import '../App.css';
import MemoryBar from './MemoryBar';
import UpgradeList from './UpgradeList';

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      manualClick: 1,
      mouseCost: 10,
      macroCount: 0,
      macroCost: 50,
      internCount: 0,
      internCost: 125,
      botCost: 350,
      botCount: 0,
      teamCount: 0,
      teamCost: 2000,
      devCost: 6500,
      devCount: 0,
      techCount: 0,
      techCost: 10000,
      superCount: 0,
      superCost: 1000000,
      quanCount: 0,
      quanCost: 1000000000,
      memory: 1000,
      memoryFactor: 10,
      bits: 0
    }
    this.addBit = this.addBit.bind(this);
    this.addMouse = this.addMouse.bind(this);
    this.addMacro = this.addMacro.bind(this);
    this.addIntern = this.addIntern.bind(this);
    this.addBot = this.addBot.bind(this);
    this.addTeam = this.addTeam.bind(this);
    this.addDev = this.addDev.bind(this);
    this.addTech = this.addTech.bind(this);
    this.addSuper = this.addSuper.bind(this);
    this.addQuan = this.addQuan.bind(this);
    this.checkMemory = this.checkMemory.bind(this);
    this.downloadRam = this.downloadRam.bind(this);
    this.addItem = this.addItem.bind(this);
    this.cosmeticRouting = this.cosmeticRouting.bind(this);
  }
 
  render() {
    return (

      
      <div className="crtScreen">
        <div className="innerCrt">
        
          <Menu/>
          
          <div className="bitButtonGroup">
            <button className="bitButton" onClick={this.addBit}>Add Bit</button>
            <p>Bits: {this.state.bits}</p>
          </div>
          <UpgradeList 
            mouseFunc={this.addMouse}
            mouseCount={this.state.manualClick}
            macroFunc={this.addMacro}
            macroCount={this.state.macroCount}
            internFunc={this.addIntern}
            internCount={this.state.internCount}
            botFunc={this.addBot}
            botCount={this.state.botCount}
            devTeamFunc={this.addTeam}
            devTeamCount={this.state.teamCount}
            devFunc={this.addDev}
            devCount={this.state.devCount}
            techFunc={this.addTech}
            techCount={this.state.techCount}
            superFunc={this.addSuper}
            superCount={this.state.superCount}
            quanFunc={this.addQuan}
            quanCount={this.state.quanCount}
            bits={this.state.bits}
            memory={this.state.memory}
          />
          <div className="memoryBar">
          <MemoryBar memory={this.state.memory} downloadFunc={this.downloadRam} cosmeticRouting={this.cosmeticRouting}/>
          </div>
        </div>
      </div>
    );
  }

  addBit() {
    this.setState((state) => ({prevBits: state.bits}));
    this.setState((state) => ({bits: state.bits + state.manualClick}));
  }

  addMouse() {
    if(this.state.bits >= this.state.mouseCost) {
      if(this.checkMemory(this.state.mouseCost)) {
        this.setState((state) => ({manualClick: state.manualClick++}));
        this.setState((state) => ({bits: state.bits-state.mouseCost}));
        this.setState((state) => ({mouseCost: Math.floor(state.mouseCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.mouseCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need at least ${this.state.mouseCost} to buy a mouse!`)
    }
  }

  addMacro() {
    if(this.state.bits >= this.state.macroCost && this.checkMemory) {
      if(this.checkMemory(this.state.macroCost)) {
        this.setState((state) => ({macroCount: state.macroCount++}));
        this.setState((state) => ({bits: state.bits-state.macroCost}));
        this.setState((state) => ({macroCost: Math.floor(state.macroCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.macroCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.macroCost} to create a Macro!`);
    }
  }

  addIntern() {
    if(this.state.bits >= this.state.internCost && this.checkMemory) {
      if(this.checkMemory(this.state.internCost)) {
        this.setState((state) => ({internCount: state.internCount++}));
        this.setState((state) => ({bits: state.bits-state.internCost}));
        this.setState((state) => ({internCost: Math.floor(state.internCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.internCost / this.state.memoryFactor)}));
      }    
    } else {
      alert(`You need ${this.state.internCost} to hire an Intern`);
    }
  }

  addBot() {
    if(this.state.bits >= this.state.botCost && this.checkMemory) {
      if(this.checkMemory(this.state.botCost)) {
        this.setState((state) => ({botCount: state.botCount++}));
        this.setState((state) => ({bits: state.bits-state.botCost}));
        this.setState((state) => ({botCost: Math.floor(state.botCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.botCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.botCost} to hire an Intern`);
    }
  }

  addDev() {
    if(this.state.bits >= this.state.devCost && this.checkMemory) {
      if(this.checkMemory(this.state.devCost)) {
        this.setState((state) => ({devCount: state.devCount++}));
        this.setState((state) => ({bits: state.bits-state.devCost}));
        this.setState((state) => ({devCost: Math.floor(state.devCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.devCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.devCost} to hire an Intern`);
    }
  }

  addTeam() {
    if(this.state.bits >= this.state.teamCost && this.checkMemory) {
      if(this.checkMemory(this.state.teamCost)) {
        this.setState((state) => ({teamCount: state.teamCount++}));
        this.setState((state) => ({bits: state.bits-state.teamCost}));
        this.setState((state) => ({teamCost: Math.floor(state.teamCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.teamCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.teamCost} to hire an Intern`);
    }
  }

  addTech() {
    if(this.state.bits >= this.state.techCost && this.checkMemory) {
      if(this.checkMemory(this.state.techCost)) {
        this.setState((state) => ({techCount: state.techCount++}));
        this.setState((state) => ({bits: state.bits-state.techCost}));
        this.setState((state) => ({techCost: Math.floor(state.techCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.techCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.techCost} to hire an Intern`);
    }
  }

  addSuper() {
    if(this.state.bits >= this.state.superCost && this.checkMemory) {
      if(this.checkMemory(this.state.superCost)) {
        this.setState((state) => ({superCount: state.superCount++}));
        this.setState((state) => ({bits: state.bits-state.superCost}));
        this.setState((state) => ({superCost: Math.floor(state.superCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.superCost / this.state.memoryFactor)}));
      }
    } else {
      alert(`You need ${this.state.superCost} to use a Supercomputer!`);
    }
  }

  addQuan() {
    if(this.state.bits >= this.state.quanCost && this.checkMemory) {
      if(this.checkMemory(this.state.quanCost)) {
        this.setState((state) => ({quanCount: state.quanCount++}));
        this.setState((state) => ({bits: state.bits-state.quanCost}));
        this.setState((state) => ({quanCost: Math.floor(state.quanCost*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state.quanCost / this.state.memoryFactor)}));
      }  
    } else {
      alert(`You need ${this.state.superCost} to build a Quantum Computer!`);
    }
  }


  // prototype - hopefully this will replace all of our repeating add code still need some work on it but want to push refactor asap
  addItem(bits, cost, count) {
    console.log('hit');
    if(this.state.bits >= {cost} && this.checkMemory) {
      if(this.checkMemory({cost})) {
        this.setState((state) => ({count: state[{count}]++}));
        this.setState((state) => ({bits: state[{bits}] - state[{cost}]}));
        this.setState((state) => ({cost: Math.floor(state[{cost}]*1.15)}));
        this.setState((state) => ({memory: state.memory - (this.state[{cost}] / this.state.memoryFactor)}));
      }
    } else alert(`You need at least ${cost} bits to purchase that!`);
  }

  // calc to check if you have enough memory for purchase - currently done through an if check maybe a better way?
  checkMemory(itemCost) {
    if(this.state.memory === 0) {
      alert('You\'re out of memory!');
      return false;
    } else if ((this.state.memory - (itemCost / this.state.memoryFactor) < 0)) {
      alert('You don\'t have enough memory for that!');
      return false;
    } else return true
  }

  // passed down as a prop to MemoryBar component
  downloadRam() {
    if(this.state.bits >= 1000) {
      this.setState((state) => ({memory: 1000}));
      this.setState((state) => ({bits: state.bits - 1000}));
      this.setState((state) => ({memory: 1000}));
      this.setState((state) => ({memoryFactor: state.memoryFactor * 1.25}));
    } else alert('You need 1000 bits to download more RAM!');
  }

  // test routing to cosmetics
  cosmeticRouting() {
    this.props.history.push('/cosmetics');
  }


  // all intervals started on mount, maybe we could start them when we purchase the items at first?
  componentDidMount() {
    this.macroInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.macroCount * 1)}));
    }, 1000);
    this.internInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.internCount * 5)}));
    }, 1000);
    this.botInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.botCount * 10)}));
    }, 1000);
    this.devInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.devCount * 100)}));
    }, 1000);
    this.teamInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.teamCount * 250)}));
    }, 1000);
    this.techInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.techCount * 1000)}));
    }, 1000);
    this.superInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.superCount * 10000)}));
    }, 1000);
    this.quanInterval = setInterval(() => {
      this.setState((state) => ({bits: state.bits+(state.quanCount * 1000000)}));
    }, 1000);

  }
  
}

export default Game;
