import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Login extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            renderForm: 0,
            username: undefined,
            password: undefined
        };

        this.newUser = this.newUser.bind(this);
        this.returningUser = this.returningUser.bind(this);
        this.createAccount = this.createAccount.bind(this);
        this.changeUsername = this.changeUsername.bind(this);
        this.changePassword = this.changePassword.bind(this);
    }

    render() {
        if(this.state.renderForm === 0) {
            return (
                <div className="initial-prompt">
                    <h1 className="welcome">Welcome to Bit Clicker!</h1>
                    <div className="form-prompt">
                        <h1 className="heading-label">Login</h1>
                        <form>
                            <div className="form-item">
                                <label>Username:</label>
                                <input type="text" onChange={this.changeUsername} className="login-input"></input>
                            </div>
                            <div className="form-item">
                                <label>Password:</label>
                                <input type="password" onChange={this.changePassword} className="login-input"></input>
                            </div>
                            <div className="form-item">
                                <button className="login-buttons" onClick={this.newUser}>New User?</button>
                                <button className="login-buttons">Continue Without Account</button>
                            </div>
                            <div className="form-item">
                                <button className="submit-account">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="initial-prompt">
                    <h1 className="welcome">Welcome to Bit Clicker!</h1>
                    <div className="form-prompt">
                        <h1 className="heading-label">Create Your Account</h1>
                        <form>
                            <div className="form-item">
                                <label>Username:</label>
                                <input type="text" className="login-input" onChange={this.changeUsername}></input>
                            </div>
                            <div className="form-item">
                                <label>Password:</label>
                                <input type="password" className="login-input" onChange={this.changePassword}></input>
                            </div>
                            <div className="form-item">
                                <button className="login-buttons" onClick={this.returningUser}>Returning User?</button>
                                <button className="login-buttons">Continue Without Account</button>
                            </div>
                            <div className="form-item">
                                <button className="submit-account" onClick={this.createAccount}>Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            );
        }
    }

    newUser(e) {
        e.preventDefault();
        this.setState({renderForm: 1});
    }

    returningUser(e) {
        e.preventDefault();
        this.setState({renderForm: 0});
    }

    createAccount(e, username, password) {
        e.preventDefault();
        let account = {
            username: this.state.username,
            password: this.state.password
        }

        console.log(account);

        axios.post('http://localhost:1323/createAccount', account).then(res => {
            console.log(res.data);
            this.props.history.push('/game');
        });
    }

    changeUsername(e) {
        this.setState({username: e.target.value});
    }

    changePassword(e) {
        this.setState({password: e.target.value});
    }

}

export default Login;