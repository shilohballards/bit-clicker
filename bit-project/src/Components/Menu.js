import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';



function Menu() {

    return (

        <div>
            <div className="menu">
                <Navbar variant="dark">
                    <Navbar.Brand href='./Components/Game'>Bit-Clicker</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="#home">Upgrades</Nav.Link>
                            <Nav.Link href="./Components/Test">Leaderboard</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        </div>


    );

}

export default Menu;