import React from 'react';


function MemoryBar(props) {
    let memory = props.memory;
    let memoryPercent = Math.floor(memory/10);

    return (
        <div>
            <div className="memoryBar">
                <h3>{memoryPercent}% Remaining Memory</h3>
                <div className="outerRectangle">
                    <div className="innerRectangle" id="innerRectangle" style={{width: memory}}></div>
                </div>
                <button className="item" onClick={props.downloadFunc} title="200">Download more RAM</button>
                <button className="item" onClick={props.cosmeticRouting}>Cosmetics</button>
            </div>
        </div>
    );
}

export default MemoryBar;